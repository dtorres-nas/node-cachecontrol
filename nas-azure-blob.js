const { BlobServiceClient, BlobClient } = require("@azure/storage-blob");

async function main() {
  console.log("Azure Blob Storage JS SDK v12");
  const AZURE_STORAGE_CONNECTION_STRING =
    process.env.AZURE_STORAGE_CONNECTION_STRING;
  const blobServiceClient = BlobServiceClient.fromConnectionString(
    AZURE_STORAGE_CONNECTION_STRING
  );
  const containerName = "activatedata";
  const containerClient = blobServiceClient.getContainerClient(containerName);

  /*
    Edit this name to target your specific folder in storage
  */
  const folderName = "Your Folder Name";

  /*
    List blobs of a specific folder

  console.log('\nListing blobs with "' + folderName + '"');
  for await (const blob of containerClient.listBlobsFlat()) {
    if (blob.name.includes(folderName)) {
      console.log("\t", blob.name);
    }
  }

  */

  /*
    Modify blobs with cache control
  */
  console.log(
    "\nAdding Cache Control Headers to all blobs in the \"" +
      folderName +
      "\" folder"
  );
  for await (const blob of containerClient.listBlobsFlat({prefix: folderName})) {
    const targetBlobClient = new BlobClient(
      AZURE_STORAGE_CONNECTION_STRING,
      containerName,
      blob.name
    );
    targetBlobClient.setHTTPHeaders({
      blobCacheControl: "public, max-age=15552000",
      blobContentType: blob.properties.contentType, // This line is required, if no value is entered it will remove the content type
    });
    console.log("\t", blob.name + " updated")
  }

  /*
    Upload text data to the container

  const blockBlobClient = containerClient.getBlockBlobClient(blobName);
  const data = "Hello, World!";
  const uploadBlobResponse = await blockBlobClient.upload(data, data.length);
  console.log(
    "Blob was uploaded successfully. requestId: ",
    uploadBlobResponse.requestId
  );
  
  */
  /*
    Upload local file, create upload folder with files to upload, loop through with Node.js if needed, but we shouldn't need to do this as you would also have to manually enter the- Content Type parameter, which could be different for each file.  Right now we are just bulk uploading files via the Storage Explorer app and using the Cache Control scripts above to bulk modify those headers (you have to manually update them in Storage Explorer)

  const uploadBlobResponse = await blockBlobClient.uploadFile(
    "./upload/filename.jpg",
    {
      blobHTTPHeaders: {
        blobCacheControl: "public, max-age=15552000",
        blobContentType: "image/jpg"
      },
    }
  );
  console.log(
    "Blob was uploaded successfully. requestId: ",
    uploadBlobResponse.requestId
  );

  */
}

main()
  .then(() => console.log("Done"))
  .catch((ex) => console.log(ex.message));
