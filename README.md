# Node.js Tool for Modifying Storage Blobs HTTP Headers

This script runs on Node.js and allows us to view, edit, delete, and upload blobs in our Azure account.

I initially used [Quickstart: Manage blobs with JavaScript v12 SDK in Node.js](https://docs.microsoft.com/en-us/azure/storage/blobs/storage-quickstart-blobs-nodejs) to build this script, but removed elements not related to our use case.  See this resource for more details on initial setup.

## The Goal
The goal of this script is to automate the process of adding in CacheControl headers to our CDN files.  Currently, if we want to bulk upload files, we can do so easily with the Azure Storage Explorer.  However, if we want to set CacheControl headers, we have to manually right click each file, scroll down a list and enter or copy/paste the settings one-by-one.

## The Solution
This script saves time by allowing us to simply enter the folder name we want to target and automatically set CacheControl headers for all files in that folder.

## How to Use
Upload files to CDN using ```{Client Name}/{Folder Name}/FileName.jpg``` format
```
git clone https://{yourAccount}@bitbucket.org/dtorres-nas/node-cachecontrol.git
cd node-cachecontrol
npm install
```
Edit folderName in ```nas-azure-blob.js```, and replace ```process.env.AZURE_STORAGE_CONNECTION_STRING``` with the raw connection string or create your own ```.env``` variable, then run:
```
node nas-azure-blob.js
```

## Requirements
- **Azure Account Credentials** - This script uses an Environment Variable to store the Connection String from Azure, however, if only using it locally, you can enter the raw string when running the script.
- **Node.js** - We should all have this already, but it's required for this script's run command.
- **Existing Blob Files** - Files must be uploaded separately for this script to be able to see them and modify them.

### Future Updates
This could be expanded to allow uploading files from the command line as well.  To be explored:

- Create list of files from local folder with Node
- Loop list of files with upload script, add in headers as needed
- Learn how virtual folders are created using this method (```{Client Name}/{Folder Name}/ActualBlob.jpg```)